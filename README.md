# Rust Scripts

These are my random rust-script scripts. These are random one-off
scripts and experiments.

These are quick and dirty scripts. They are not going to be the best
quality Rust code.

## about the creation of the repo

For reference, this repo was created using the `nix flake init` command:

```sh
nix flake init -t templates#rust
```

I removed the `naersk` input from the `flake.nix` because I am not using
a proper crate for this project.

I also add `openssl` to the `mkShell nativeBuildInputs` list and
`pkg-config` to the `mkShell buildInputs` because crates such as
reqwests require it.
