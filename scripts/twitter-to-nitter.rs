#!/usr/bin/env rust-script
//! This scripts converts a user's follows into a list of nitter RSS
//! URLs. This is for migrating off of Twitter but to continue keeping
//! tabs on people and entities that use Twitter to make announcements.
//!
//! I created this because a number of activists only use twitter to
//! announce actions and mutual aid opportunities. I didn't want to
//! lose that connection when I left Twitter.
//!
//! ```cargo
//! [dependencies]
//! twitter-v2 = "0.1.8"
//! tokio = { version = "1", features = ["full"] }
//! log = "0.4.17"
//! env_logger = "0.9.1"
//! ```
use twitter_v2::TwitterApi;
use twitter_v2::authorization::BearerToken;
use std::error;
use log;

type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    let username = std::env::var("TWITTER_USERNAME").expect("TWITTER_USERNAME");
    log::info!("using username: {:?}", username);

    let auth = BearerToken::new(std::env::var("APP_BEARER_TOKEN").unwrap());
    let api = TwitterApi::new(auth);

    let user = api.get_user_by_username(username)
        .send()
        .await?
        .into_data()
        .expect("user not found");

    log::info!("got user {:?}", user);

    let follows = api.get_user_following(user.id)
        .max_results(1000)
        .send()
        .await?
        .into_data()
        .expect("followers not found");

    println!(r#"<?xml version="1.0" encoding="utf-8"?>
<opml version="2.0">
  <body>"#);

    for follow in follows.iter() {
        println!(r#"    <outline type="rss" xmlUrl="https://nitter.net/{}/rss" />"#, follow.username);
    }

    println!(r#"  </body>
</opml>"#);

    Ok(())
}
